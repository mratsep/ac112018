var gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  browserify = require('gulp-browserify'),
  browserSync = require('browser-sync').create(),
  reload = browserSync.reload;

gulp.task('serve', ['scss'], function() {
  browserSync.init({
    snippetOptions: {
      rule: {
        match: /<\/head>/i,
        fn: function (snippet, match) {
          return snippet + match;
        }
      }
    },
    notify: false,
    server: {
      baseDir: "./"
    }
  });
  gulp.watch('assets/scss/*.scss', ['scss']);
  gulp.watch('js/main.js').on('change', browserSync.reload);
  gulp.watch('*.html').on('change', browserSync.reload);
});

gulp.task('scss', function() {
  return gulp.src("assets/scss/*.scss")
  .pipe(sass().on('error', sass.logError))
  .pipe(autoprefixer({
    browsers: ['last 4 versions', 'IE 8', '>5%'],
    remove: false
  }))
  .pipe(gulp.dest("assets/css"))
  .pipe(reload({stream:true}));
});

gulp.task('default', ['serve']);
