$(function() {
  $('.js-ac-patches-filter-item').click(function(e) {
    e.preventDefault();
    $('.js-ac-patches-filter-item').removeClass('m-active');
    $(this).addClass('m-active');
  });

  $('.js-patch-option').click(function(e) {
    e.preventDefault();
    var msg = $(this).data('message');
    $(this).closest('.js-patch-item-main').find('.ac-patch-list-item-main-options-item').removeClass('m-active');
    $(this).addClass('m-active');
    $(this).closest('.js-patch-item-main').addClass('m-active').find('.js-patch-item-message').html('<span>' + msg + '</span>');
  });

  $('.js-patch-preview').click(function(e) {
    e.preventDefault();
    var src = $(this).data('src');
    var filename = $(this).data('filename');
    $('body').append('<div class="ac-patch-preview"><div class="ac-patch-preview-main"><a href="#" class="ac-patch-preview-main-close js-patch-preview-close"><span>Close</span></a><figure class="ac-patch-preview-main-image"><img src="' + src + '" alt="Patch Image"></figure><div class="ac-patch-preview-main-title"><span>' + filename + '</span></div></div></div>');
  });

  $(document).on('click', '.js-patch-preview-close', function(e) {
    e.preventDefault();
    $('.ac-patch-preview').addClass('m-fadeOut');
    setTimeout(function() {
      $('.ac-patch-preview').remove();
    }, 1000);
  });
});
